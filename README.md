# Admin Toolbar Language Switcher

Add the Toolbar Language Switcher to the administration toolbar
of the project.
Ease visualization of the current page language context and handy
navigation view for change to another language.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/toolbar_language_switcher).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/toolbar_language_switcher).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

No requirements at this time.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Currently no configuration exists.


## Maintainers

[//]: # "cspell:disable"
- Oleh Vehera - [voleger](https://www.drupal.org/u/voleger)
- James Williams - [james.williams](https://www.drupal.org/u/jameswilliams-0)
- Roman Hryshkanych - [Romixua](https://www.drupal.org/u/romixua)
- Mykola Veryha - [MykolaVeryha](https://www.drupal.org/u/mykolaveryha)
